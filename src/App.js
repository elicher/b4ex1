import logo from './logo.svg';
import './App.css';
import React from 'react'

class App extends React.Component {
  state = {
    counter:0
  }
  count = () => {
    let { counter } = this.state;
    counter += 1;
    this.setState({ counter })
  }


  render () {
    return (
    <div className="App">
    {(this.state.counter%2==0) ? <h2>You clicked me {this.state.counter} times</h2> : <h2> </h2>}
    <buttons onClick={this.count}>THIS IS A BUTTON</buttons>
  </div>)}
  }

export default App;
