import { configure, mount } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });

const app = mount(<App />);

describe("b4ex01: Odd Counter", () => {
  it("There should be a button with an onClick event", () => {
    let isThereAButton = false;
    const button = app.find("button");
    if (button.prop("onClick") !== undefined) {
      isThereAButton = true;
    }
    if (!isThereAButton) return false;
  });
  it("There should be an h2 with initial value 0", () => {
    expect(parseInt(app.find("h2").text())).toEqual(0);
  });
  it("After pressing button 2 times, h2 should hold value: 1", async () => {
    const button = app.find("button");
    button.simulate("click");
    button.simulate("click");
    app.setProps({});
    expect(parseInt(app.find("h2").text())).toEqual(1);
  });
});
